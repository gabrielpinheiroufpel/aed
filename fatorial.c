#include "fatorial.h"

	int fatorial(int x){
	
	int fat = 1, cont;
	
	if(x == 0 || x < 0)
	{
		return -1;
	}
	
	else if(x == 1)
	{
		return 1;
	}
	
	else {
		for(cont = x; cont > 1; cont--)
		{
			fat *= cont;
		}
		return fat;
	}

}
